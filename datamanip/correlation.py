#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Correlations, mutual informations, ... and anything about comparison of two series.
"""

import numpy as np

corr_registry = {}
def register_corr(func):
    """
    Define a decorator writting the name of the correlation methods and a callable argument.
    For use in other modules.
    """
    corr_registry[str(func.__name__.split('_')[-1])] = func
    return(func)

def coeff_corr(x1,x2,name='s'):
    """
    name='p' -> Pearson coefficient
    name='s' -> Székely distance
    """
    assert len(x1) == len(x2), 'Same length for the two series is required'
    l=len(x1)
    if name == 'p':
        x1_m = np.mean(x1)
        x2_m = np.mean(x2)
        A = x1-x1_m
        B = x2-x2_m
    elif name == 's':
        y1 = x1.reshape(l,1)
        y2 = x1.reshape(1,l)
        a = np.abs(y1-y2)
        y0 = np.mean(a,axis=0).reshape(l,1)
        y1 = np.mean(a,axis=1).reshape(1,l)
        ym = np.mean(a)
        A  = a-y0-y1+ym
        y1 = x2.reshape(l,1)
        y2 = x2.reshape(1,l)
        b = np.abs(y1-y2)
        y0 = np.mean(b,axis=0).reshape(l,1)
        y1 = np.mean(b,axis=1).reshape(1,l)
        ym = np.mean(b)
        B = b-y0-y1+ym
    else:
        return("Did not understood the name of the method.")
    S0 = np.sum(A*B)
    S = np.sum(A*A)*np.sum(B*B)
    S = np.sqrt(S, where=S!=0.0)
    r = np.divide(S0, S, where=S!=0.0)
    return(r)

@register_corr
def corr_pearson(x1,x2):
    """
    Calculate the Pearson correlation coefficient, as defined in
    https://en.wikipedia.org/wiki/Pearson_correlation_coefficient
    """
    return(coeff_corr(x1,x2, name='p'))

@register_corr
def corr_szekely(x1,x2):
    """
    Calculate the distance correlation, as defined by Szekély, see
    https://en.wikipedia.org/wiki/Distance_correlation
    """
    return(coeff_corr(x1,x2, name='s'))

@register_corr
def corr_matthews(x1,x2):
    """
    Calculate the Matthews correlation coefficient for two-sided classification problems.
    https://en.wikipedia.org/wiki/Matthews_correlation_coefficient
    The two series must be of same length, and must have only two categories +/-1.
    In that case, the Matthews correlation is just the Pearson correlation of the sign of the series.
    https://leimao.github.io/blog/Matthews-Correlation-Coefficient/
    """
    x1 = np.sign(x1)
    x2 = np.sign(x2)
    return(coeff_corr(x1,x2,name='p'))

#@register_corr
def corr_szekely_sign(x1,x2):
    """
    Calculate the distance correlation, as defined by Szekély, see
    https://en.wikipedia.org/wiki/Distance_correlation
    But for the sign of the series.
    """
    x1 = np.sign(x1)
    x2 = np.sign(x2)
    return(coeff_corr(x1,x2, name='s'))
    
#def corr_matthews(x1,x2):
#    """
#    Calculate the Matthews correlation coefficient for two-sided classification problems.
#    https://en.wikipedia.org/wiki/Matthews_correlation_coefficient
#    The two series must be of same length, and must have only two categories.
#    """
#    assert len(x1) == len(x2), 'Same length for the two series is required'
#    s1 = np.sign(x1)
#    s2 = np.sign(x2)
#    m1 = (s1 + 1)//2
#    m2 = (s2 + 1)//2
#    TP = np.sum(m1*m2)
#    TN = np.sum((1-m1)*(1-m2))
#    FP = np.sum(m1*(1-m2))
#    FN = np.sum((1-m1)*m2)
#    r0 = TP*TN - FP*FN
#    r1 = (TP+FP)*(TP+FN)*(TN+FP)*(TN+FN)
#    r1 = np.sqrt(r1, where=r1!=0.0)
#    r = np.divide(r0, r1, where=r1!=0.0)
#    return(r)

def get_rank(x):
    """
    Ranking of an array, from
    https://stackoverflow.com/a/5284703/8844500
    """
    temp = x.argsort()
    ranks = np.zeros(len(temp), dtype=int)
    ranks[temp] = np.arange(len(x))
    return(ranks)

def rank_corr(x1,x2,name='s'):
    """
    Generalized rank correlation coefficient, 
        if name='k', calculate the Kendall
        if name='s', calculate the Spearman
    https://en.wikipedia.org/wiki/Rank_correlation#General_correlation_coefficient
    for more information
    """
    assert len(x1) == len(x2), 'Same length for the two series is required'
    r1 = get_rank(x1)
    r2 = get_rank(x2)
    l = len(x1)
    y1 = r1.reshape(l,1)
    y2 = r1.reshape(1,l)
    z1 = r2.reshape(l,1)
    z2 = r2.reshape(1,l)
    if name=='k':
        A = np.asarray(np.sign(y1-y2), dtype=float)
        B = np.asarray(np.sign(z1-z2), dtype=float)
    elif name=='s':
        A = np.asarray(y2-y1, dtype=float)
        B = np.asarray(z2-z1, dtype=float)
    else:
        return("Argument Error")
    r0 = np.sum(A*B)
    r = np.sum(A*A)*np.sum(B*B)
    r = np.sqrt(r, where=r!=0.0)
    r = np.divide(r0, r, where=r!=0.0)
    return(r)

@register_corr
def rank_kendall(x1,x2):
    return(rank_corr(x1,x2, name='k'))

@register_corr
def rank_spearman(x1,x2):
    return(rank_corr(x1,x2, name='s'))

def random_choice_correlation():
    """
    Randomly choose one of the correlation coefficient of the present module.
    Use the decorator @register_corr
    """
    methods = list(corr_registry.keys())
    m = np.random.randint(len(methods))
    method = methods[m]
    return(method)

def simple_matching_coefficient(x1,x2):
    """
    Calculate the single matching coefficient between two series.
    https://en.wikipedia.org/wiki/Simple_matching_coefficient
    """
    assert len(x1) == len(x2), 'Same length for the two series is required'
    l = len(x1)
    s1 = np.sign(x1)
    s2 = np.sign(x2)
    r = s1*s2 + 1
    r = np.sum(r)/l
    return(r)
    
if __name__ == '__main__':
    import pandas as pd
    dic = {str(i+1): np.random.random(size=1043) for i in range(10)}
    df = pd.DataFrame(dic)
    index = pd.date_range(start='2000-01-01', periods=df.shape[0], freq='B')
    df.index = index
    df = df.sort_index()
    names = {k: v for k,v in corr_registry.items() if k not in ['szekely','matthews']}
    for k,v in names.items():
        corr1 = df.corr(method=v)
        corr2 = df.corr(method=k)
        minus = corr1.to_numpy() - corr2.to_numpy()
        mean = np.mean(minus)
        std = np.std(minus)
        print(k+' : \t %.4f +/- %.4f' %(mean, std))
    
    for i in range(10):
        x1 = np.random.random(1000)-0.5
        x2 = np.random.random(1000)-0.5
        m = corr_matthews(x1,x2)
        print("Matthews coefficient for random series =\t %.4f" %(m))
        m = corr_pearson(np.sign(x1),np.sign(x2))
        print("Pearson coefficient for random series =\t\t %.4f" %(m))
    
    for i in range(0,1000,100):
        x1 = np.random.random(1000)-0.5
        choice = np.random.choice(np.arange(len(x1)), size=i, replace=False)
        x2 = np.random.random(1000)-0.5
        x2[choice] = x1[choice]
        s = corr_szekely_sign(x1,x2)
        print("Szekely distance for random +/-1 series with %i replacements =\t %.4f" %(i,s))
