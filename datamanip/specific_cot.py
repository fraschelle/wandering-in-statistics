#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The generic manipulation of DataSeries
"""

import numpy as np
import pandas as pd

import datamanip.generic as dgen

#%%
## to check : 
#N = 1000
#a = np.random.random(N)
#b = np.random.random(N)
#df = pd.DataFrame([a,b]).T
#df.columns=['a', 'b']
#ddf = df.copy()
#
#df0 = create_index(df,p=20)
#df[df0.columns] = df0

def create_net(df_cot):
    """
    Take a COT table, and generate the net positions per actor (Managed Money, Non Reportable, Producer/Merchent, ...) in addition to the percentage of open interests per population.
    Returns a Pandas DataFrame containing only the created columns.
    """
    df0 = df_cot.copy()
    prefixes = ['m_money-positions', 'nonrept-positions', 'other_rept-positions', 'swap-positions', 'prod_merc-positions']
    for p in prefixes:
        df0[p+'-net'] = df0[p+'-long']-df0[p+'-short']
    df0 = dgen.drop_columns(df0, df_cot)
    return(df0)

def create_percent(df_cot):
    """
    Take a COT table, and generate the net positions per actor (Managed Money, Non Reportable, Producer/Merchent, ...) in addition to the percentage of open interests per population.
    Returns a Pandas DataFrame containing only the created columns.
    """
    df0 = df_cot.copy()
    prefixes = ['m_money-positions', 'nonrept-positions', 'other_rept-positions', 'swap-positions', 'prod_merc-positions']
    # to create a series filled with zeros
    df0['tot'] = df0[prefixes[-1]+'-long'] - df0[prefixes[-1]+'-long']
    for p in prefixes:
        try: df0[p+'tot'] =  df0[p+'-long']+df0[p+'-short']+2*df0[p+'-spread']
        except KeyError: 
            df0[p+'tot'] =  df0[p+'-long']+df0[p+'-short']
        # column 'tot' of open interests, to calculate the percentage
        df0['tot'] = df0['tot'] + df0[p+'tot']
        df0 = df0.drop(columns=[p+'tot'])
    for p in prefixes:
        pp = p[:-10]
        # calculate the percentage of open interest per population of actors 
        try: 
            df0[pp+'-pct_of_oi'] =  (df0[p+'-long']+df0[p+'-short']+2*df0[p+'-spread'])/(2*df0['tot'])
        except KeyError: 
            df0[pp+'-pct_of_oi'] =  (df0[p+'-long']+df0[p+'-short'])/(2*df0['tot'])
    df0 = df0.drop(columns=['tot'])
    df0 = dgen.drop_columns(df0, df_cot)
    return(df0)


