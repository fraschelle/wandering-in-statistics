#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some tools to convert some datas into some other, like
 - pandas DataFrame to numpy arrays (conserving columns and index) -> df_to_dict or df_to_tuple
 - dictionnary of list to list of dictionnaries -> dictlist_to_listdict
"""

import numpy as np
import pandas as pd


def dictlist_to_listdict(dl):
    """
    Take a dictionnary where a few keys are given in the form of list,
    and returns a list of dictionnaries, where all keys are now a single entry,
    and all combinations of the previous elements of the lists are present.
    If there is no list in the dictionnary, the function returns the initial dictionnary.
    """
    assert type(dl) is dict, "Input must be a dictionnary"
    kl = [k for k in dl.keys() if type(dl[k]) is list]
    if len(kl) == 0:
        ld = dl.copy()
    else:
        d0 = {k:dl[k] for k in dl.keys() if k not in kl}
        l0 = [(v,) for v in dl[kl[0]]]
        for k in kl[1:]:
            l0 = [(*v,w) for v in l0 for w in dl[k]]
        ld = [{kl[i]: tup[i] for i in range(len(kl))} for tup in l0]
        ld = [{**d, **d0} for d in ld]
    return(ld)

def listdict_to_dictlist(ld):
    """
    Transform a list of dictionaries with all the same keys into a dictionnary of lists.
    One list per keys of the initial dictionnary.
    """
    assert len(ld) > 0, "Input must have a length"
    assert type(ld) is list, "Input must be a list"
    assert type(ld[0]) is dict, "Elements of the input list must be dictionnaries"
    d = {k:[v] for k,v in ld[0].items()}
    for dic in ld[1:]:
        assert type(dic) is dict, "Elements of the input list must be dictionnaries"
        for q,w in dic.items():
            d[q].append(w)
    return(d)

def extract_array(df):
    """
    Extract the array from the DataFrame
    """
    if float(pd.__version__[:4]) >= 0.24:
        array = df.to_numpy()
    else:
        if len(list(df.columns)) > len(list(df.index)):
            array = np.array([df.iloc[i,:] for i in range(df.shape[0])])
        else:
            array = np.array([df.iloc[:,j] for j in range(df.shape[1])]).T
    return(array)

def df_to_dict(df):
    """
    Take a pandas DataFrame, and returns a dictionnary, whose datas are organized as
     - array: the numpy array of the datas, with shape (index, columns)
     - columns: the list of columns of the numpy array 
     - index: the list of raws of the array
    Reconstructing the DataFrame from the df_to_dico function consists in asking
      df = pandas.DataFrame(dic['array'], columns=dic['columns'], index=dic['index'])
    where 
      dic = df_to_dict(df)
    """
    index = list(df.index)
    columns = list(df.columns)
    array = extract_array(df)
    dic = dict(array=array, index=index, columns=columns)
    return(dic)

def df_to_tuple(df):
    """
    Take a pandas DataFrame, and returns a tuple, whose datas are organized as
     - 0: the numpy array of the datas, with shape (index, columns)
     - 1: the list of columns of the numpy array 
     - 2: the list of raws of the array
    Reconstructing the DataFrame from the df_to_dico function consists in asking
      df = pandas.DataFrame(tup[0], columns=tup[1], index=tup[2])
    where 
      dic = df_to_tuple(df)
    """
    index = list(df.index)
    columns = list(df.columns)
    array = extract_array(df)
    tup = tuple(array, columns, index, )
    return(tup)