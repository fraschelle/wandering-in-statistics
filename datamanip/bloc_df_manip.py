#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
All dependence of shuffling some time series, and recasts them.
"""

import numpy as np
import pandas as pd

def fill_df(df, freq=None):
    """
    Fill a pandas DataFrame according to its index.
    One can either impose a frequency to the pandas.date_range, or use the frequency of the original DataFrame df.
    Method to fill is forward fill from pandas.
    """
    df0 = df.copy().sort_index()
    dmin = df0.index.min()
    dmax = df0.index.max()
    if freq is None:
        f0 = getattr(df0.index,'freq',None)
        if f0 is None: 
            raise AttributeError("The DataFrame has no frequency")
        else:
            index = pd.date_range(start=dmin, end=dmax, freq=f0)
    else:
        index = pd.date_range(start=dmin, end=dmax, freq=freq)
    df0 = pd.DataFrame(df0, index=index).fillna(method='ffill')
    return(df0)

def compress_ohlc(df, freq='W-FRI'):
    """
    Compress the daily OHLC datas to weekly OHLC.
    The close will be the close of the day before the frequency.
    The open will be the open of the week.
    The high and low are the max and min of the week.
    Volume is the sum of the volume of the week.
    Open_interest if the open_interest of the week.
    Works also for a lower frequency (as month BM, BMS, year Y)
    
    Parameters
    ----------
    df: a pandas DataFrame, with a business day frequency. Otherwise the DataFrame will be aligned on a business day frequency
    freq: a string representing a frequency, documented in 
        https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases
    
    Return
    ------
    A DataFrame with lower frequency than df
    """
    df0 = df.copy().sort_index()
    f = getattr(df.index,'freq',None)
    if f is None:
        index = pd.date_range(start=df0.index[0], end=df0.index[-1], freq='B')
        df0 = df0.reindex(index)
    index = pd.date_range(start=df0.index[0], end=df0.index[-1], freq=freq)
    df_ohlc = []
    for i in range(1,len(index)):
        dmin = index[i-1]
        dmax = index[i]
        dftemp = df0.loc[dmin:dmax,:].dropna()
        dic = {}
        dic['date'] = dmax
        try: dic['close'] = dftemp.iloc[-1].loc['close']
        except KeyError: pass
        try: dic['open'] = dftemp.iloc[0].loc['open']
        except KeyError: pass
        try: dic['high'] = dftemp.loc[:,'high'].max()
        except KeyError: pass
        try: dic['low'] = dftemp.loc[:,'low'].min()
        except KeyError: pass
        try: dic['volume'] = dftemp.loc[:,'volume'].sum()
        except KeyError: pass
        try: dic['open_interest'] = dftemp.loc[:,'open_interest'].sum()
        except KeyError: pass
        try: dic['openinterest'] = dftemp.loc[:,'openinterest'].sum()
        except KeyError: pass    
        df_ohlc.append(dic)
    df0 = pd.DataFrame(df_ohlc,index=index[1:])
    check = index[1:] == df0['date']
    assert np.mean(check)==1.0, "Problem with indexing"
    df0 = df0.drop(columns=['date'])
    return(df0)

def resample_ohlc(df, freq='W-WED'):
    ohlc_dict = {                                                                                                             
    'open':'first',                                                                                                    
    'high':'max',                                                                                                       
    'low':'min',                                                                                                        
    'close': 'last',                                                                                                    
    }
    df0 = df.resample(freq, closed='left', label='left').apply(ohlc_dict)
    return(df0)

def add_returns_for_shuffling(df, cols=['close']):
    """
    Complete all the dates of a DataFrame whose index is a time series, fill the NaN with the previous datas, and calculate the returns associated to the cols (a list of string).
    Works better for frequency related index of DataFrame.
    If cols is an empty list, the function calculates the returns for all columns.
    All the columns gets the preffix 'returnshuff-' in front of their initial name.
    """
    df0 = df.copy()
    if len(cols) == 0: 
        cols = list(df0.columns)
    for c in cols:
        df0['returnshuff-'+c] = df0[c].diff(1)
    df0 = df0.drop(columns=cols)
    return(df0)

def recombine_dfs(df1,df0):
    """
    Recombine the DataFrame after one calculated the returns of the original series, and separate the original DataFrame in blocs.
    df1.shape[0] = df2.shape[0] -1 
    """
    index = df0.index
    cols = [c.split('-')[1:] for c in df1.columns]
    cols = ['-'.join(c) for c in cols]
    df1.columns = cols
    df0.columns = cols
    df1 = df1.append(df0.iloc[0]).sort_index()
    df0 = np.cumsum(df1)
    df0.index = index
    return(df0)

def iterate_df_fold(df,n_splits):
    """
    Splits a DataFrame of length N into n_splits folds.
    Returns an iterable with the folds of the DataFrame.
    """
    df0 = df.copy().sort_index()
    n_samples = df0.shape[0]
    if n_splits > n_samples:
        raise ValueError(
            ("Cannot have number of splits n_splits={0} greater"
             " than the number of samples: n_samples={1}.")
            .format(n_splits, n_samples))
    fold_sizes = np.full(n_splits, n_samples//n_splits, dtype=np.int)
    fold_sizes[:n_samples%n_splits] += 1
    start = 0 
    for fold_size in fold_sizes:
        start, stop = start, start + fold_size
        yield df0.iloc[start:stop]
        start = stop

def shuffle_df_fold(df,n):
    """
    Cut a pandas DataFrame in n blocs, and shuffle these blocs.
    Returns a pandas DataFrame with the same index, same column names, but shuffled blocs of datas.
    """
    df0 = df.copy().sort_index()
    index = df0.index
    dfolds = [dfold for dfold in iterate_df_fold(df0,n)]
    l = len(dfolds)
    indics = np.random.permutation(l)
    dfolds = [dfolds[i] for i in indics]
    df0 = pd.concat(dfolds, axis=0)
    df0.index = index
    return(df0)

def rotate_df_fold(df,n):
    """
    Cut a pandas DataFrame in n blocs, and make a circular perutation of the blocs.
    """
    df0 = df.copy().sort_index()
    index = df0.index
    dfolds = [dfold for dfold in iterate_df_fold(df0,n)]
    temp = dfolds[0]
    dfolds[0] = dfolds[-1]
    dfolds[-1] = temp
    df0 = pd.concat(dfolds, axis=0)
    df0.index = index
    return(df0)

def shuffle_df(df,n):
    """
    Shuffle a DataFrame by cutting the time index in n parts, then shuffling these parts, and reassambling the values from a frame of returns.
    """
    df0 = fill_df(df)
    df1 = add_returns_for_shuffling(df0, cols=[])
    df1 = shuffle_df_fold(df1.dropna(),n)
    df0 = recombine_dfs(df1,df0)
    return(df0)

def rotate_df(df,n):
    """
    Cut a DataFrame in n coniguous blocs, then put the last bloc to the first position.
    The time series behavior is mimic thanks to first order difference with the previous value of th eoriginal DataFrame.
    """
    df0 = fill_df(df)
    df1 = add_returns_for_shuffling(df0, cols=[])
    df1 = rotate_df_fold(df1.dropna(),n)
    df0 = recombine_dfs(df1,df0)
    return(df0)

def monte_carlo_time_series(df, N, n):
    """
    Shuffle N times all the columns with column-name 'returnshuff-[...]' of a DataFrame, which shuffling size n.
    Returns a dictionnary of all the shuffled columns.
    """
    a = np.zeros([N,df.shape[0],df.shape[1]])
    for i in range(N):
        a[i] = shuffle_df(df,n).to_numpy()
    return(a)

def stats(array):
    """
    Dictionnary of statistics
    """
    dic = {}
    for n in ['mean', 'median', 'min', 'max', 'std']:
        dic[n] = getattr(np,n)(array, axis=0)
    translation = {'95%': 2, '50%': 0.4775, '75%': 0.8135, }
    for k,v in translation.items():
        dic[k+'_confidence_up'] = dic['mean'] + v*dic['std']
        dic[k+'_confidence_down'] = dic['mean'] - v*dic['std']
    return(dic)