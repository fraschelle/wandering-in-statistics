#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some ways of generating ranking for the analysis of predictor outcomes.
"""

import numpy as np

def ranking(dic):
    """
    Rank the score of a dictionnary, trying to maximize the 'mean', 'min' and 'max' entries, and minimizes the 'std' and 'amp' entries of the dictionnary.
    """
    assert type(dic) is dict, "Input must be a dictionnary"
    list_max = ['mean','min','max',]
    list_min = ['std','amp',]
    keys = list(dic.keys())
    n = len(keys)
    ranks = np.zeros(n, dtype=int)
    score = {k: 0 for k in keys}
    m = 0
    for c in list_max:
        datas = [dic[k][c] for k in keys]
        args = np.argsort(datas)   
        ranks[args] = np.arange(n)
        score = {keys[i]: score[keys[i]]+ranks[i] for i in range(n)}
        m += 1
    for c in list_min:
        datas = [dic[k][c] for k in keys]
        args = np.argsort(datas)   
        ranks[args] = np.arange(n)
        score = {keys[i]: score[keys[i]]-ranks[i]+n-1 for i in range(n)}
        m += 1
    score = {k: v/(m*n) for k,v in score.items()}
    datas = [score[k] for k in keys]
    score = [(keys[i],score[keys[i]]) for i in np.argsort(datas)]
    return(score[::-1])

def ranking_list(l):
    """
    Rank the score of a list of statistical dictionnaries, trying to maximize the 'mean', 'min' and 'max' entries, and minimizes the 'std' and 'amp' entries of the dictionnary elements of the list.
    
    Parameters
    ----------
    The list should reads in the form:
        [{'mean':..., 'min':..., 'max':...,}, {'mean':..., }, ...]
    and the function classifies the elements of the list
    
    Result
    ------
    A list of the same length as the entry list.
    Each element of the list is a two-tuple, with entry 
        (position in ranking, score)
    """
    assert type(l) is list, "Input must be a list"
    list_max = ['mean','min','max',]
    list_min = ['std','amp',]
    n = len(l)
    ranks = np.zeros(n, dtype=int)
    score = ranks
    m = 0
    for c in list_max:
        datas = [d[c] for d in l]
        args = np.argsort(datas)   
        ranks[args] = np.arange(n)
        score = score+ranks
        m += 1
    for c in list_min:
        datas = [d[c] for d in l]
        args = np.argsort(datas)   
        ranks[args] = np.arange(n)
        score = score+ranks
        m += 1
    score = score/(m*n)
    score = [(i,score[i]) for i in np.argsort(score)]
    return(score[::-1])

def combine_rankings(ranks, weights=None):
    if weights is None:
        weights = np.full(len(ranks),1)
    assert len(ranks)==len(weights), "List of ranks and list of weights must have the same length"
    score = {r[0]: r[1]*weights[0] for r in ranks[0]}
    for i in range(1,len(ranks)):
        score = {r[0]: score[r[0]]+r[1]*weights[i] for r in ranks[i]}
    w = np.sum(weights)
    score = {k: v/w for k,v in score.items()}
    keys = list(score.keys())
    datas = [score[k] for k in keys]
    score = [(keys[i],score[keys[i]]) for i in np.argsort(datas)]
    return(score[::-1])

