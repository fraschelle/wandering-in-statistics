"""
Some useful statistical ditributions, and some tools to fit them.
"""

import numpy as np
from scipy.optimize import fsolve
from scipy.special import iv

def gumbel_density(x,mu,beta):
    """
    Plots a Gumbel distribution, with parameters [mu,beta] as defined on
        https://en.wikipedia.org/wiki/Gumbel_distribution
    """
    z = (x-mu)/beta
    p = np.exp(-z+np.exp(-z))/beta
    return(p)

def gumbel_pdfit(series):
    """
    Fits a sample to a Gumbel distribution, and extracts the parameters mu and beta, as defined on
        https://en.wikipedia.org/wiki/Gumbel_distribution
    """
    m = np.mean(series)
    v = np.var(series)
    beta = np.sqrt(6*v)/np.pi
    mu = m - beta*np.euler_gamma
    return([mu,beta])

def gaussian_density(x,mu,sigma):
    """
    Plots the gaussian / normal probability distribution from a coordinate set x and the two parameters [mu,sigma] defined on 
        https://en.wikipedia.org/wiki/Normal_distribution
    Input : 
        - x, a 1D Numpy array
        - mu, a float
        - sigma, a float
    Output : a 1D Numpy array
    """
    numer = np.exp(-(x-mu)**(2)/(2*sigma**2))
    denom = np.sqrt(2*np.pi*sigma**2)
    return(numer/denom)

def gaussian_pdfit(series):
    """
    Fit a sample to a Gaussian probability distribution, from a 1D Numpy array, it returns [mu,sigma], as defined on 
        https://en.wikipedia.org/wiki/Normal_distribution
    """
    m = np.mean(series)
    v = np.var(series)
    return([m,np.sqrt(v)])

def lognormal_density(x,mu,sigma):
    """
    Plots the log-normal probability distribution from a coordinate set x and the two parameters [mu,sigma] defined on 
        https://en.wikipedia.org/wiki/Log-normal_distribution
    Input : 
        - x, a 1D Numpy array
        - mu, a float
        - sigma, a float
    Output : a 1D Numpy array
    """
    x = np.array(x)
    numer = np.exp(-(np.log(x)-mu)**(2)/2/sigma**2)
    denom = x*sigma*np.sqrt(2*np.pi)
    return(numer/denom)

def lognormal_pdfit(series):
    """
    Extract the most probable fit parameters from a sample supposed to represent a log-normal probability distribution.
    Input : a 1D Numpy series
    Output, mean mu and standard deviation sigma of the log-normal distribution.
    mu and sigma are defined on 
        https://en.wikipedia.org/wiki/Log-normal_distribution
    """
    m = np.mean(series)
    v = np.var(series)
    sigma2 = np.log(1+v/m/m)
    mu = np.log(m) - sigma2/2
    return([mu,np.sqrt(sigma2)])

def vonmises_density(x,mu,kappa):
    """
    Calculate the von Mises density for a series x (a 1D numpy.array).
    Input : 
        x : a 1D numpy.array of size L
        mu : a 1D numpy.array of size n, the mean of the von Mises distributions
        kappa : a 1D numpy.array of size n, the dispersion of the von Mises distributions
    Output : 
        a (L x n) numpy array, L is the length of the series, and n is the size of the array containing the parameters. Each row of the output corresponds to a density
    """
    res = []
    for i in x:
        f = np.exp(kappa*np.cos(i-mu))
        n = 2*np.pi*iv(0,kappa)
        res.append(f/n)
    return(np.array(res))

def vonmises_pdfit(series):
    """
    Calculate the estimator of the mean and deviation of a sample, for a von Mises distribution
    Input : 
        series : a 1D numpy.array
    Output : 
        the estimators of the parameters mu and kappa of a von Mises distribution, in an list [mu, kappa]
    See https://en.wikipedia.org/wiki/Von_Mises_distribution 
    for more details on the von Mises distribution
    """
    s0 = np.mean(np.sin(series))
    c0 = np.mean(np.cos(series))
    mu = np.arctan2(s0,c0)
    var = 1-np.sqrt(s0**2+c0**2)
    k = lambda kappa: (1-var)*iv(0,kappa)-iv(1,kappa)
    kappa = fsolve(k, 0.0)[0]
    return([mu,kappa,var])

def least_square_periodic(x,y):
    """
    Apply the least square method for a linear fit of a sample, periodic in the y variable with period 2*numpy.pi
    """
    xm = np.mean(x)
    ym = np.arctan2(np.mean(np.sin(y)),np.mean(np.cos(y)))
    numer = (x-xm)*np.arctan2(np.sin(y-ym),np.cos(y-ym))
    denom = (x-xm)**2
    a = np.sum(numer)/np.sum(denom)
#    a = np.arctan2(np.sin(a),np.cos(a))
    b = ym - a*xm
#    b = np.arctan2(np.sin(b),np.cos(b))
    return([a,b])

def hellinger_dist(x1,x2):
    """
    Hellinger distance between the two samples of same size x1 and x2.
    See https://en.wikipedia.org/wiki/Hellinger_distance for definitions
    Input : x1, x2 : lists or numpy 1D arrays
    Output : A float number
    The two series must be of same length and normalised, i.e. 
        numpy.sum(x1) = numpy.sum(x2) = 1.0
        len(x1) == len(x2) is True
    """
    h = np.sqrt(x1)-np.sqrt(x2)
    h = np.sum(h**2)
    h = np.sqrt(h/2)
    return(h)

def histo(X,nb_bars = 10):
    """
    Constructs the bar chart of the series X, from its max to its min by step given by the number of bars nb_bars
    Input : 
        a 1D numpy.array X, or a list
        an integer nb_bars
    Output : a 2D numpy array, [bins, occurences]
    """
    X    = np.array(X)
    xmax = np.max(X)
    xmin = np.min(X)
    i    = xmin
    pas  = (xmax-xmin)/nb_bars
    res  = []
    pos  = []
    n    = 0
    while i <= xmax and n < nb_bars:
        a1 = len(X[X>=i])
        a2 = len(X[X>=i+pas])
        res.append(a1-a2)
        pos.append(i+pas/2)
        i += pas
        n += 1
    h = np.stack([pos,res], axis=1)
    return(h)
