#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some statistical tools for the analysis.
"""

import numpy as np
import scipy.stats as ss

def binomial_statistics(x):
    """
    For a +/-1 random series, return the estimator of the binomial probability, its confidence interval, and the mean and variance of its credible interval.
    """
    dic = {}
    x = np.sign(x)
    p = (1+np.mean(x))/2
    dic['p_inf'] = p
    dic['std_inf'] = np.sqrt((p*(1-p)/2+1)/4/len(x))
#    print("Probability to get +1: %.2f +/- %.2f" %(dic['p_inf'],dic['std_inf']))
    dic['B_test'] = ss.binom_test(np.sum(x>0), len(x), p, alternative='two-sided')
    return(dic)

def binomial_comparison(x1,x2):
    """
    If two random series of +/-1 trials are supposed to be independent, what is the expectation value and variance of the two series to give indentical values at the same time ? 
    """
    dic = {}
    assert len(x1) == len(x2), "Series must be of same length"
    p1 = binomial_statistics(x1)['p_inf']
    p2 = binomial_statistics(x2)['p_inf']
    q1 = 1-p1
    q2 = 1-p2
    dic['mean_inf'] = (p1*p2+q1*q2)
    dic['var_inf'] = (p1*p2+q1*q2)*(1-p1*p2-q1*q2)/len(x1)
    dic['r'] = np.sqrt(p1*q1*p2*q2)
#    print("Probability of identical values: %.2f +/- %.2f" %(dic['mean_inf'],1.96*np.sqrt(dic['var_inf'])))
    return(dic)

def simple_stats(l):
    """
    Calculate some simple statistics of a list of data.
    Returns a dictionnary.
    """
    assert type(l) is list, "Input must be a list"
    keys = ['mean','median','std','min','max',]
    dic0 = {k:getattr(np,k)(l) for k in keys}
    dic0['amp'] = dic0['max']-dic0['min']
    keys = [55,65,75,85,95,]
    dic1 = {'p_'+str(k):getattr(np,'percentile')(l,k) for k in keys}
    dic2 = {'size':len(l)}
    return({**dic0,**dic1,**dic2})

def combine_stats(dic1, dic2):
    """
    If two lists of sample datas from the same random variable have been synthethized using simple_stats, this function calculate the simple_stats associated to the combination of both.
    Note that the percentile (including the median) can not be calculated from their relatives, so they are excluded from the returned dictionnary of this function.
    """
    dic = {}
    l1, l2 = dic1['size'], dic2['size']
    dic['size'] = l1+l2
    m1, m2 = dic1['mean'], dic2['mean']
    m = (l1*m1 + l2*m2)/(l1+l2)
    dic['mean'] = m
    s1, s2 = dic1['std']**2, dic2['std']**2
    s = (l1*s1+l2*s2+l1*(m1-m)**2+l2*(m2-m)**2)/(l1+l2)
    dic['std'] = np.sqrt(s)
    keys = ['min', 'max']
    dmm = {k: getattr(np,k)([dic1[k],dic2[k]]) for k in keys}
    dmm['amp'] = dmm['max']-dmm['min']
    return({**dic,**dmm})

def combine_list_of_stats(dics):
    """
    Collapse a list of dictionnaries of statistics into a single dictionnary of statistics.
    This is usefull when making a list of simple_stats for samples of different sizes.
    
    Parameters
    ----------
    A list of dictionnaries, in the format of simple_stats outcome
    
    Return
    ------
    A dictionnary with combine mean, standard-deviation, min and max.
    The outcome is in the same format as combine_stats
    """
    assert type(dics) is list, "Input must be a list of dictionnaries"
    if len(dics) == 1:
        res = dics
    elif len(dics) == 2:
        res = combine_stats(dics[0],dics[1])
    else:
        res = combine_stats(dics[0],dics[1])
        for dic in dics[2:]:
            res = combine_stats(res,dic)
    return(res)

#%%

if __name__ == '__main__':
    x1 = np.sign(np.random.random(150)-0.7)
    x2 = np.sign(np.random.random(150)-0.5)
    
    dic = binomial_comparison(x1,x2)
    
    binomial_statistics(x2)['B_test']
