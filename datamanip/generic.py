#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The generic manipulation of DataSeries.
All the functions which transform a pandas DataFrame begin with the 'create_' string.
So one can call all these functions using 

import datamanip.generic as dgen
l_func = dir(dgen)
l_func = [l for l in l_func if l.startswith('create_')]

All these functions return a new pandas DataFrame with the transformed series and without the initial series.
A decorator 'generic_datamanip_registry' also catch all the manipulation names in a dictionnary
"""

import numpy as np
import pandas as pd

import datamanip.correlation as corr

generic_datamanip_registry = {}
def datamanip_registry(func):
    """
    Define a decorator writting the name of the manipulation methods and a callable argument in a dictionnary.
    For use in other modules.
    """
    generic_datamanip_registry[str(func.__name__)] = func
    return(func)

def select_columns(df, items=None, like=None, regex=None, exclude=False):
    """
    Use the fiter function of pandas to select some columns.
    """
    if (items is None) & (regex is None) & (like is None) :
        # Default : Keep everything
        filter_args = {'items' : df.columns}
    elif like :
        # Apply `like` first. Keep all columns who satisfy 'like in column_name'
        filter_args = {'like' : like}
    elif regex :
        # Apply `regex`. Keep all columns who satisfy the regex
        filter_args = {'regex' : regex}
    elif len(items) > 0 :
        # Apply `item`. Keep all columns in the list 'items'
        filter_args = {'items' : items}

    if exclude :
        exclude_cols = df.head(0).filter(**filter_args , axis=1).columns
        keep_cols = [col for col in df.columns if not col in exclude_cols]
        df_filtered = df[keep_cols]
    else :
        df_filtered = df.filter(**filter_args , axis=1)
    return df_filtered

def drop_columns(df1, df2):
    """
    Drop all columns of df1 which are also present in df2
    Returns df1 without the extra columns.
    df1 has more columns than df2, otherwise the function returns df1
    """
    df0 = df1.copy()
    cols = [c for c in list(df2.columns) if c in list(df0.columns)]
    df0 = df0.drop(columns=cols)
    return(df0)

def give_suffix(x):
    """
    Function tool for later use.
    Returns a specific suffix for the name of the column, depending on the sign of the argument.
    Entry : a number (float, integer)
    Return : a string 'm' or empty for negative/positive entry.
    """
    if np.sign(x)<0: 
        k='m' 
    else: 
        k=''
    return(k)

def count_values(l0):
    """
    Count the number of occurence of an element in a list.
    Return a dictionnary with key the list element and values the number of time the element appears.
    """
    dic = {}
    for l in l0:
        try:
            dic[l] += 1
        except KeyError:
            dic[l] = 1
    return(dic)

@datamanip_registry
def create_shifts(df, shifts=[1,2,3,4,5,]):
    """
    Shift all the columns by the shifts in a list.
    """
    df0 = df.copy()
    for c in df0.columns:
        for s in shifts:
            k = give_suffix(s)               
            df0[c+'-shift'+k+str(abs(s))] = df0[c].shift(s)
    df0 = drop_columns(df0,df)
    return(df0)

@datamanip_registry
def create_return_direction(df, shifts=[5,10,15,]):
    """
    Sign of the returns of the datas with respect to the shifts given in a list.
    An absence of change is characterized as a decrease (that is np.sign can give either +/-1 or 0, and 0 is transformed to -1)
    """
    df0 = df.copy()
    for c in df0.columns:
        for s in shifts:
            k = give_suffix(s)
            df0[c+'-retdir'+k+str(abs(s))] = np.sign(df0[c].diff(s))
            mask = df0[c+'-retdir'+k+str(abs(s))] == 0
            df0.loc[mask,c+'-retdir'+k+str(abs(s))] = np.sign(-0.01)
    df0 = drop_columns(df0,df)
    return(df0)
        
def inflexion_points(series):
    """
    Take a series 's' of 3 numbers, and returns
     +1 when s[0]<s[1] and s[1]>s[2] : resistance
     -1 when s[0]>s[1] and s[1]<s[2] : support
     0 otherwise
    """
    s = series[1:] - series[:-1]
    s = np.sign(s)
    p = s[0]*(1-s[0]*s[1])//2
    return(p)

@datamanip_registry
def create_inflexion_points(df):
    """
    Support and resistance of the DataFrame
    """
    df0 = df.copy()
    for c in df0.columns:
        df0[c+'-inflex'] = df0[c].rolling(3).apply(inflexion_points, raw=True)
    df0 = drop_columns(df0,df)
    return(df0)

@datamanip_registry
def create_sma(df,periods=[6,8,12,]):
    """
    Simple moving average of the series, with period given in a list
    """
    df0 = df.copy()
    for c in df0.columns:
        for p in periods:
            k = give_suffix(p)
            df0[c+'-sma'+k+str(p)] = df0[c].rolling(p).mean()
    df0 = drop_columns(df0,df)
    return(df0)

@datamanip_registry
def create_ewm(df,periods=[6,8,12,]):
    """
    Exponential moving average of the series, with period given in a list
    """
    df0 = df.copy()
    for c in df0.columns:
        for p in periods:
            k = give_suffix(p)               
            df0[c+'-ewa'+k+str(p)] = df0[c].ewm(span=p).mean()
    df0 = drop_columns(df0,df)
    return(df0)

@datamanip_registry
def create_returns(df,shifts=[7,14,21,]):
    """
    Difference with the previous data in the DataFrame
    """
    df0 = df.copy()
    for c in df0.columns:
        for s in shifts:
            k = give_suffix(s)               
            df0[c+'-return'+k+str(abs(s))] = df0[c].diff(s)
    df0 = drop_columns(df0,df)
    return(df0)

def stochastic_index(series):
    numer = series[-1] - np.min(series)
    denom = np.max(series) - np.min(series)
    return(numer/denom)

@datamanip_registry
def create_stochastic_index(df,periods=[104, 222, 545,]):
    """
    Difference with the historical maximimum over the max-min, rolling over the period in a list.
    Be warn that the windows size onto which the rolling is applied is p+1 instead of p, to avoid problems in case one chooses p=1, since there is a zero division in that case.
    """
    df0 = df.copy()
    for c in df0.columns:
        for p in periods:
            k = give_suffix(p)            
            df0[c+'-index'+k+str(p+1)] = df0[c].rolling(window=p+1).apply(stochastic_index, raw=True)
    df0 = drop_columns(df0,df)
    return(df0)

@datamanip_registry
def create_product_series(df):
    """
    Take a Pandas DataFrame of numerical series, and calculate the products among the series.
    Supposed to give arguments for non-linearities to later treatement of the datas.
    """
    check = [df[c].dtype.kind in 'bifc' for c in df]
    assert np.prod(check) == True, 'Some series are not numerical'    
    df0 = df.copy()
    cols = []
    for i in df0.columns:
        df0[str(i+'_prod_'+i)] = df0[i]*df0[i]
        cols.append(i)
        avoid_cols = [c for c in df.columns if c not in cols]
        for j in avoid_cols:
            df0[str(i+'_prod_'+j)] = df0[i]*df0[j]
    df0 = drop_columns(df0,df)
    return(df0)

@datamanip_registry
def create_sum_series(df):
    """
    Take a Pandas DataFrame of numerical series, and calculate the sums among the series.
    Supposed to give arguments for corss dependent linearities to later treatement of the datas.
    """
    check = [df[c].dtype.kind in 'bifc' for c in df]
    assert np.prod(check) == True, 'Some series are not numerical'    
    df0 = df.copy()
    cols = []
    for i in df0.columns:
        df0[str(i+'_sum_'+i)] = df0[i]+df0[i]
        cols.append(i)
        avoid_cols = [c for c in df.columns if c not in cols]
        for j in avoid_cols:
            df0[str(i+'_sum_'+j)] = df0[i]+df0[j]
    df0 = drop_columns(df0,df)
    return(df0)

#@datamanip_registry
#def create_diagonal_direction_series(df, method=None):
#    """
#    Calculate the correlation matrix between all the columns of a pandas DataFrame, then calculate the linear superpositions which diagonalise the correlation matrix.
#    'method' could be either
#     - 'pearson', check linearity between the series
#     - 'szekely', check independence between the series
#     - 'kendall', check rank sign between the series
#     - 'spearman', check rank ordering between the series
#    if 'method' is left empty (or method=None), the correlation is chosen randomly among the above list.
#    """
#    df0 = df.copy()
#    df1 = df.copy()
#    if not method: method = corr.random_choice_correlation()
#    c = df0.corr(method=corr.corr_registry[method])
#    name = method
#    v,w = np.linalg.eigh(np.array(c))
#    for i in range(len(v)): df1[name+'_'+str(i)] = np.dot(df0, w[:,i])
#    df0 = drop_columns(df1,df)
#    return(df0)
#
#@datamanip_registry
#def create_diagonal_products_direction_series(df):
#    """
#    First compute the products between all the series in the pandas DataFrame, then apply the function 'diagonal_direction_series' to combine the series.
#    """
#    df0 = df.copy()
#    df1 = create_product_series(df0)
#    df0[df1.columns] = df1
#    df1 = create_diagonal_direction_series(df0)
#    df1.columns = df1.columns.map(lambda x: x+'_prod')
#    return(df1)

@datamanip_registry
def create_log_returns(df, shifts=[1,2,3,4,5,]):
    """
    Calculate the logarithm series in the pandas DataFrame, then substract the previous logarithm shifted by shift in the list.
    Do not produce anything if the series contains negative values.
    """
    df0 = df.copy()
    for c in df0.columns:
        if np.sum(df0.loc[:,c] <= 0.0):
            pass
        else:
            for s in shifts:
                k = give_suffix(s)
                df0[c+'-logret'+k+str(abs(s))] = np.log(df0.loc[:,c]).diff(s)
    df0 = drop_columns(df0,df)
    return(df0)

@datamanip_registry
def create_rate(df, shifts=[1,2,3,4,5,]):
    """
    Calculate the rate between a date and a previous one, shifted by shifts in a list.
    A rate is defined as p[i]/p[i-shift].
    If a rate is infinite in a column, drop the column.
    """
    df0 = df.copy()
    for c in df0.columns:
        for s in shifts:
            k = give_suffix(s)            
            df0[c+'-rate'+k+str(abs(s))] = df0.loc[:,c]/df0.loc[:,c].shift(s)
    df0 = drop_columns(df0,df)
    for c in df0.columns:
        if np.sum(df0.loc[:,c] == np.inf):
            df0 = df0.drop(columns=c)
    return(df0)

@datamanip_registry
def create_normalized_rate(df, shifts=[1,2,3,4,5,]):
    """
    Calculate the normalized rate between a date and a previous one, shifted by shifts in a list.
    A rate is defined as (p[i]/p[i-shift]-1).
    Can be also calculated using 
     -> np.expm1(np.log(df0.loc[:,c]).diff(s))
    instead of 
     -> (df0.loc[:,c]/df0.loc[:,c].shift(s) -1)
    """
    df0 = df.copy()
    for c in df0.columns:
        for s in shifts:
            k = give_suffix(s)            
            df0[c+'-normrate'+k+str(abs(s))] = (df0.loc[:,c]/df0.loc[:,c].shift(s) -1)
    df0 = drop_columns(df0,df)
    for c in df0.columns:
        if np.sum(df0.loc[:,c] == np.inf):
            df0 = df0.drop(columns=c)
    return(df0)

#def create_several_at_once(df, keep=True, *list_tuples):
#    """
#    Calculate several series from df, when 
#     list_tuples is a list of tuples organized as
#     ('name_of_the_function', list_of_shifts)
#    'name_of_the_function' is the name of the function without 'create_'
#     for instance 'normalized_rate'
#    list_of_shift is a list of arguments to be given as shifts or periods
#     for instance [1,2,3,4,5,]
#    """
#    df0 = df.copy()
#    
#    if keep == True:
#        df0 = pd.concat([df0,*dfs], axis=1, join='inner')
#    else:
#        df0 = pd.concat(dfs, axis=1, join='inner')
#    return(df0)

if '__name__' == '__main__':    
    dic = {str(i+1): np.random.random(size=1043) for i in range(10)}
    df = pd.DataFrame(dic)
    index = pd.date_range(start='2000-01-01', periods=df.shape[0], freq='B')
    df.index = index
    df = df.sort_index()
    df1 = create_log_returns(df)
