#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Linear regression tools
"""

import numpy as np

def least_square_periodic(x,y):
    """
    Apply the least square method for a linear fit of a sample, periodic in the y variable with period 2*numpy.pi
    """
    xm = np.mean(x)
    ym = np.arctan2(np.mean(np.sin(y)),np.mean(np.cos(y)))
    numer = (x-xm)*np.arctan2(np.sin(y-ym),np.cos(y-ym))
    denom = (x-xm)**2
    a = np.sum(numer)/np.sum(denom)
#    a = np.arctan2(np.sin(a),np.cos(a))
    b = ym - a*xm
#    b = np.arctan2(np.sin(b),np.cos(b))
    return([a,b])
