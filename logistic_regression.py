#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
All about the perceptron and the logistic regression.



Main sources of documentation : 
 - https://en.wikipedia.org/wiki/Logistic_regression
 - https://en.wikipedia.org/wiki/Perceptron
"""

import numpy as np
from scipy.optimize import root

import numpy.random as rnd
from statistics_tools import plots as plt
from statistics_tools.sample_draws import bernouilli as ber

y = ber(100,0.3)
np.mean(y)
np.std(y)

plt.display_statistics(y)

y = rnd.logistic()

data = np.genfromtxt('logistic_regression.csv', delimiter=',')
x = data[:,0:-1]
y = data[:,-1].reshape(x.shape[0],1)


def add_x0(x):
    """
    Add a first column of 1 on the data, in order to fit the regression coefficients.
    
    Parameters:
        x: a 2D numpy array, with data in columns (first argument of x.shape is the larger)
    
    Output:
        a numpy array, of shape (x.shape[0], x.shape[1] +1)
    """
    assert x.ndim == 2, "Input array must be of dimension 2"
    z = np.full(x.shape[0],1).reshape(x.shape[0],1)
    z = np.concatenate([z,x], axis=1)
    return z

def p(beta,x):
    """
    Calculate the density probability of a logistic regression, of coefficients beta and data x
    """
    assert beta.shape[0] == x.shape[1], "Wrong match of the array, len of beta must be the same of the number of columns in x. Try transposing x for testing."
    z = np.sum(beta*x, axis=1)
    z = 1/(1+np.exp(-z))
    return z

def eqs(beta,x,y):
    """
    Equation to be resolved for the logistic regression

    Parameters
    ----------
    beta : a 1D numpy array, or a list
        the parameter of the logistic regression, which have to be found by optimization protocol.
    x : a 2D numpy array
        the data.
    y : a 1D numpy array
        the target of the regression.

    Returns
    -------
    z : a 1D numpy array
        the numerical equations corresponding to the maximization of the likelihood.
        one value per entry in the beta array

    """
    z = p(beta,x)
    z = y.flatten()-z
    z = np.tile(z,(2,1)).T
    z = np.sum(x*z, axis=0)
    return z

def jac(beta,x,y):
    """
    The Jacobian of the likelihood, for use in the root finding algorithm
    """
    jacobian = np.zeros((x.shape[1],x.shape[1]))
    dp = p(beta,x)*(1-p(beta,x))
    for i in range(x.shape[1]):
        for j in range(x.shape[1]):
            jacobian[i,j] = np.sum(x[:,i]*x[:,j]*dp)
    return -jacobian

def logistic_regression(x,y,start=None):
    """
    Calculate the coefficients of the logistic regression, with data x and target y.

    Parameters
    ----------
    x : a numpy array
        the data, in columns.
    y : a numpy array
        the target, incolumns.
    start : a list or a numpy array, optional
        Starting guess of the parameters, in order to initialize the root finding algorithm.
        Must be of the length of x.shape[1]. 
        The default is None, in which case the initial guess are random.

    Returns
    -------
    coeffs : dictionnary of arrays
        coeffs['x'] corresponds to the coefficients of the regression, in the form
            [intercept, beta1, beta2, ...]
        coeffs['fjac'] corresponds to the jacobian at the convergence point
    
    Returns an error in case of non convergence of the root finding algorithm.

    """
    if start is None:
        b0 = np.random.random(size=x.shape[1])
    else:
        b0 = start
    x = add_x0(x)
    roots = root(eqs, b0, args=(x,y), jac=jac)
    print(roots.message)
    if roots.status==1:
        coeffs = dict(x=roots.x,
                      fjac=roots.fjac,
                      )
    return coeffs
    

b0 = np.random.random(size=2)
# minim = minimize(log_likelihood, b0, args=(x,y), method='Nelder-Mead', tol=1e-6)

# minim = minimize(log_likelihood, b0, args=(x,y), method='BFGS', jac=jac_ll, options={'gtol': 1e-6, 'disp': True})
# minim.message
# print(minim)

roots = root(eqs, b0, args=(x,y), method='hybr', jac=None)

roots = root(eqs, b0, args=(x,y), jac=jac)

b1 = roots.x
p(b1, x)

p0 = (p(b0,x)*(1-p(b0,x))).reshape(x.shape[0],1)


plt.scatter(x[:,1],y)
plt.plot(x[:,1],p(b1,x))
plt.show()
plt.clf()

import matplotlib.pyplot as plt
b1 = np.linspace(-5,-2,10)
b2 = np.linspace(0,12,100)
for b in b1:
    f = [jac_ll([b,t],x,y) for t in b2]
    f = np.array(f)
    for i in range(f.shape[1]):
        plt.plot(b2,f[:,i])
        plt.title('beta[%s] for b=%s' %(str(i),str(b)[:4]))
        plt.show()
        plt.clf()


#%%
from sklearn.linear_model import LogisticRegression

lr = LogisticRegression(penalty='none',random_state=None)
x = x[:,1].reshape(x.shape[0],1)
lr.fit(x,y)
lr.get_params()

pred = lr.predict(x)
predp = lr.predict_proba(x)

plt.scatter(x,y)
plt.plot(x,predp[:,1])
