#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some statistical tools for the analysis.
"""

import numpy as np
import scipy.stats as ss

def binomial_statistics(x):
    """
    For a +/-1 random series, return the estimator of the binomial probability, its confidence interval, and the mean and variance of its credible interval.
    """
    dic = {}
    x = np.sign(x)
    p = (1+np.mean(x))/2
    dic['p_inf'] = p
    dic['std_inf'] = np.sqrt((p*(1-p)/2+1)/4/len(x))
#    print("Probability to get +1: %.2f +/- %.2f" %(dic['p_inf'],dic['std_inf']))
    dic['B_test'] = ss.binom_test(np.sum(x>0), len(x), p, alternative='two-sided')
    return(dic)

def binomial_comparison(x1,x2):
    """
    If two random series of +/-1 trials are supposed to be independent, what is the expectation value and variance of the two series to give indentical values at the same time ? 
    """
    dic = {}
    assert len(x1) == len(x2), "Series must be of same length"
    p1 = binomial_statistics(x1)['p_inf']
    p2 = binomial_statistics(x2)['p_inf']
    q1 = 1-p1
    q2 = 1-p2
    dic['mean_inf'] = (p1*p2+q1*q2)
    dic['var_inf'] = (p1*p2+q1*q2)*(1-p1*p2-q1*q2)/len(x1)
    dic['r'] = np.sqrt(p1*q1*p2*q2)
#    print("Probability of identical values: %.2f +/- %.2f" %(dic['mean_inf'],1.96*np.sqrt(dic['var_inf'])))
    return(dic)

