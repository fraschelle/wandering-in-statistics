#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Statistical distances
"""

def hellinger_dist(x1,x2):
    """
    Hellinger distance between the two samples of same size x1 and x2.
    See 
     - https://en.wikipedia.org/wiki/Hellinger_distance 
    for definitions
    Input : x1, x2 : lists or numpy 1D arrays
    Output : A float number
    """
    assert len(x1) == len(x2), "The two series must have the same length to be compared"
    h = np.sqrt(x1)-np.sqrt(x2)
    h = np.sum(h**2)
    h = np.sqrt(h/2)
    return(h)