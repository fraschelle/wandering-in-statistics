#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Random sampling, from scratch

Most of the function in this module are already implemented in numpy, see
 - https://docs.scipy.org/doc/numpy-1.14.0/reference/routines.random.html
(eventually change the version of numpy)
"""

import numpy as np
import numpy.random as rnd

def bernouilli(N,p=0.2):
    """
    Returns a Bernouilli sample of {0,1} outcome with probability p of getting 1.
    
    Parameters
    ----------
    N : integer
        Size of the sample to be drawn.
    p : float (0<p<1), optional
        Probability of getting an outcome of 1. The default is 0.2.

    Returns
    -------
    y : a Numpy array
        The sample drawn.

    """
    assert (p>0) and (p<1), "0<p<1 is a probability"
    y = rnd.random(size=N)-(1-p)
    y = (np.sign(y)+1)//2
    return y
