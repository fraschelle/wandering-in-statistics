#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tools to plots some draw of random sample.
"""

import numpy as np
import matplotlib.pyplot as plt

def display_statistics(x, bins=None, display=True):
    """
    Calculate the estimated histograms (using numpy.histogram function), and returns the distribution of a 1D numpy array
    
    Parameters
    ----------
    x : numpy array or list
        DESCRIPTION.
    bins : integer, optional
        number of bins in the estimators, or a range. 
        The default is 10.
    display : boolean
        Display or not the estimators. The default is True.

    Returns
    -------
    A dictionnary with entries 'histogram', 'frequencies', 'cumldist' and 'bins'.
    All are 1D numpy arrays of equal length.

    """
    assert x.ndim==1, "Requires 1D numpy array"
    res = {}
    if not bins:
        bins = 10
    histo, bins = np.histogram(x, bins=bins)
    res['histogram'] = histo
    bins = (bins[:-1]+bins[1:])/2
    res['bins'] = bins
    freqs = histo/np.sum(histo)
    res['frequencies'] = freqs
    cdf = np.cumsum(freqs)
    res['cumuldist'] = cdf
    if display is True:
        plt.plot(bins, freqs)
        plt.ylabel('Frequencies')
        plt.xlabel('bins')
        plt.title('Histogram, in frequency')
        plt.show()
        plt.clf()
        plt.plot(bins, cdf)
        plt.ylabel('Density of probability')
        plt.xlabel('bins')
        plt.title('Cumulative distribution function')
        plt.show()
        plt.clf()
    return res


