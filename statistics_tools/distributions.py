#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some useful statistical ditributions, and some tools to fit them.
"""

import numpy as np
from scipy.optimize import fsolve
from scipy.special import iv

def gaussian_density(x,mu,sigma):
    """
    Plots the gaussian / normal probability distribution from a coordinate set x and the two parameters [mu,sigma] defined on 
        https://en.wikipedia.org/wiki/Normal_distribution
    Input : 
        - x, a 1D Numpy array
        - mu, a float
        - sigma, a float
    Output : a 1D Numpy array
    """
    numer = np.exp(-(x-mu)**(2)/(2*sigma**2))
    denom = np.sqrt(2*np.pi*sigma**2)
    return(numer/denom)

def gaussian_pdfit(series):
    """
    Fit a sample to a Gaussian probability distribution, from a 1D Numpy array, it returns [mu,sigma], as defined on 
        https://en.wikipedia.org/wiki/Normal_distribution
    """
    m = np.mean(series)
    v = np.var(series)
    return([m,np.sqrt(v)])

def gumbel_density(x,mu,beta):
    """
    Plots the density function of a Gumbel distribution, with parameters [mu,beta] as defined on
        https://en.wikipedia.org/wiki/Gumbel_distribution
    """
    x = np.array(x)
    z = (x-mu)/beta
    p = np.exp(-z+np.exp(-z))/beta
    return p

def gumbel_pdfit(series):
    """
    Fits a sample to a Gumbel distribution, and extracts the parameters mu and beta, as defined on
        https://en.wikipedia.org/wiki/Gumbel_distribution
    """
    m = np.mean(series)
    v = np.var(series)
    beta = np.sqrt(6*v)/np.pi
    mu = m - beta*np.euler_gamma
    return([mu,beta])

def logistic_density(x, loc, scale):
    """
    Plots the density function of a logistic distribution, as defined on
        https://en.wikipedia.org/wiki/Logistic_distribution
    """
    x = np.array(x)
    numer = np.exp((loc-x)/scale)
    denom = scale*(1+np.exp((loc-x)/scale))**2
    return numer/denom

def lognormal_density(x,mu,sigma):
    """
    Plots the log-normal probability distribution from a coordinate set x and the two parameters [mu,sigma] defined on 
        https://en.wikipedia.org/wiki/Log-normal_distribution
    Input : 
        - x, a 1D Numpy array
        - mu, a float
        - sigma, a float
    Output : a 1D Numpy array
    """
    x = np.array(x)
    numer = np.exp(-(np.log(x)-mu)**(2)/2/sigma**2)
    denom = x*sigma*np.sqrt(2*np.pi)
    return numer/denom

def lognormal_pdfit(series):
    """
    Extract the most probable fit parameters from a sample supposed to represent a log-normal probability distribution.
    Input : a 1D Numpy series
    Output, mean mu and standard deviation sigma of the log-normal distribution.
    mu and sigma are defined on 
        https://en.wikipedia.org/wiki/Log-normal_distribution
    """
    m = np.mean(series)
    v = np.var(series)
    sigma2 = np.log(1+v/m/m)
    mu = np.log(m) - sigma2/2
    return([mu,np.sqrt(sigma2)])

def vonmises_density(x,mu,kappa):
    """
    Calculate the von Mises density for a series x (a 1D numpy.array).
    Input : 
        x : a 1D numpy.array of size L
        mu : a 1D numpy.array of size n, the mean of the von Mises distributions
        kappa : a 1D numpy.array of size n, the dispersion of the von Mises distributions
    Output : 
        a (L x n) numpy array, L is the length of the series, and n is the size of the array containing the parameters. Each row of the output corresponds to a density
    """
    res = []
    for i in x:
        f = np.exp(kappa*np.cos(i-mu))
        n = 2*np.pi*iv(0,kappa)
        res.append(f/n)
    return np.array(res) 

def vonmises_pdfit(series):
    """
    Calculate the estimator of the mean and deviation of a sample, for a von Mises distribution
    Input : 
        series : a 1D numpy.array
    Output : 
        the estimators of the parameters mu and kappa of a von Mises distribution, in an list [mu, kappa]
    See https://en.wikipedia.org/wiki/Von_Mises_distribution 
    for more details on the von Mises distribution
    """
    s0 = np.mean(np.sin(series))
    c0 = np.mean(np.cos(series))
    mu = np.arctan2(s0,c0)
    var = 1-np.sqrt(s0**2+c0**2)
    k = lambda kappa: (1-var)*iv(0,kappa)-iv(1,kappa)
    kappa = fsolve(k, 0.0)[0]
    return([mu,kappa,var])
