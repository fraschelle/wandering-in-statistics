#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 14:14:18 2019

@author: francois
"""
import ARMA
import numpy as np
import matplotlib.pyplot as plt
from math import gamma
import os
#%%
# AR process
N = 800
d = {1:0.0, 2:0.0, 30:0.0, 80:0.0}
X = ARMA.ar(d,N)
plt.plot(X)
print('Mean = \t\t'+str(np.mean(X)))
print('Standard dev =\t'+str(np.std(X)))
#%%
# autocorrelation function of AR process
x = ARMA.autocorr(X)
plt.plot(x)
y = ARMA.bartlett_filter(x,q=50)
plt.plot(y)
#%%
# spectrum of AR process
f1, s1 = ARMA.sample_spect(x)
plt.plot(f1,s1)
f2, s2 = ARMA.sample_spect(y)
plt.plot(f2,s2)
#%%
# periodogramme of AR process
p1, s1 = ARMA.sample_spect_period(x,500)
plt.plot(p1,s1)
p2, s2 = ARMA.sample_spect_period(y,500)
plt.plot(p2,s2)
#%%
# AR vs Nabla.AR process
X = ARMA.ar(d,N)
f1, s1 = ARMA.sample_spect_period(ARMA.autocorr(X),N)
f2, s2 = ARMA.sample_spect_period(ARMA.autocorr(ARMA.nabla(X,1)),N)
f3, s3 = ARMA.sample_spect_period(ARMA.autocorr(ARMA.fracdiff(X,0.2)),N)
plt.plot(f1,s1)
plt.plot(f2,s2)
plt.plot(f3,s3)
#%%
# generate 10 different AR processes, all of period 30 days
for i in range(0,20):
    X = ARMA.ar(d,N)
    x = ARMA.autocorr(X)
    p, s = ARMA.sample_spect_period(x,200,precision=0.1)
    data = np.stack((p,s), axis=1)
    np.save(str(i),data)
names = [i for i in os.listdir() if i[-3:] == 'npy']
tot = np.load(names[0])
# take the average of all the previous spectra 
for n in names[1:]:
    temp = np.load(n)
    tot[:,1] += temp[:,1]
plt.plot(tot[:,0],tot[:,1]/len(names))
#%%
# test of the differentiation
for i in range(2,10):
    x = [k**i for k in range(0,100)]
    for j in range(0,i):
        y = ARMA.nabla(x,j)
        plt.plot(y, label=j)
        plt.legend(loc='upper left')
    plt.show()
    print(ARMA.nabla(x,i)[:10])
    print(gamma(i+1))
#%%
x = [np.exp(k) for k in range(0,100)]
for i in range(0,10):
    y = ARMA.nabla(x,i)
    z = [y[k]-x[k+i] for k in range(0,len(y))]
    print('Difference = ('+str(np.mean(z))+'+/-'+str(np.std(z))+')')
#%%
# mean of an AR process
d = {1:0.2}
m = []
for i in range(0,500):
    x = ARMA.ar(d,5000)
    m.append(np.mean(x))
#%%
plt.title('Means of a sample of 500 AR(1) processes')
plt.xlabel('nb. of sample')
plt.ylabel('mean')
y = np.arange(0,len(m))
plt.scatter(y,m)
m_mean = [np.mean(m) for i in y]
m_p = [m_mean + np.std(m) for i in y]
m_m = [m_mean - np.std(m) for i in y]
plt.plot(y,m_mean,'r--')
plt.plot(y,m_p,'g:')
plt.plot(y,m_m,'g:')
plt.show()
plt.clf()
#%%
x,y = ARMA.histo(m,20)
plt.scatter(x,y)
#%%
# fractional brownian motion
N = 500
d = 0.4
X = ARMA.fract_rand(d,N)
plt.plot(X)
print(np.mean(X))
print(np.std(X))
#%%
d = 0.3
N = 5000
m = []
for i in range(0,500):
    x = ARMA.fract_rand(d,N)
    m.append(np.mean(x))
plt.scatter(np.arange(0,len(m)),m)
print('Mean = \t('+str(np.mean(m))+' +/- '+str(np.std(m))+')')
#%%
# ARMA random
a = 0.2      # a float
b = 0.9       # a float
p = 15         # an integer
q = 80        # an integer
N = 1500      # total number of points generated, N > max(p,q)
X = ARMA.arma_rand(a,b,p,q,N)
plt.plot(X)
print(np.mean(X))
print(np.std(X))