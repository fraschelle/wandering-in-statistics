#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 14:14:18 2019

@author: francois
"""
import ARMA
import numpy as np
import matplotlib.pyplot as plt
from math import gamma
import os
#%%
def pearson(x1,x2):
    """
    Calculate the Pearson coefficient between the two series.
    See formula on
        https://en.wikipedia.org/wiki/Pearson_correlation_coefficient#For_a_sample
    $$$ LaTeX code : 
        r_{xy} =\frac{\sum ^n _{i=1}(x_i - \bar{x})(y_i - \bar{y})}{\sqrt{\sum ^n _{i=1}(x_i - \bar{x})^2} \sqrt{\sum ^n _{i=1}(y_i - \bar{y})^2}
    $$$
    Input : two lists, or 1D numpy.array
    Output : a float
    """
    x1_m = np.mean(x1)
    x2_m = np.mean(x2)
    s1 = 0
    s2 = 0
    for i in range(len(x1)):
        s1 += (x1[i]-x1_m)**2
        s2 += (x2[i]-x2_m)**2
    a1 = 0
    for i in range(len(x1)):
        a1 += (x1[i]-x1_m)*(x2[i]-x2_m)
    return(a1/np.sqrt(s1)/np.sqrt(s2))
#%%
# AR process
N = 800
d = {1:0.5, 2:0.0, 30:0.0, 80:0.0}
X = ARMA.ar(d,N)
plt.plot(X)
print('Mean = \t\t'+str(np.mean(X)))
print('Standard dev =\t'+str(np.std(X)))
#%%
# Pearson coefficients for the series, the autorrelation function and the spectrum with respect to the dimension of the fractional differentiation
N = 1000
d = {1:0.45}
Q = []
for i in range(100):
    ser = []
    aut = []
    spe = []
    Z = []
    z = 0.0
    while z<1:
        X = ARMA.ar(d,N)
        x = X
        y = ARMA.fracdiff(X,z)
        ser.append(pearson(x,y))
        a1 = ARMA.autocorr(x)
        a2 = ARMA.autocorr(y)
        aut.append(pearson(a1,a2))
        f1,s1 = ARMA.sample_spect_period(a1,N/4)
        f2,s2 = ARMA.sample_spect_period(a2,N/4)
        spe.append(pearson(s1,s2))
        Z.append(z)
        z += 0.05
    q = np.stack([Z,ser,aut,spe], axis=0)
    Q.append(q)
np.save('statistics',Q)
#%%
q = np.array(Q)
for v in q:
    for k in range(1,4):
        plt.plot(v[0],v[k])
    plt.show()
#%%
mean = np.mean(q, axis=0)
std  = np.std (q, axis=0)
# plot the data
plt.title('Correlation between original and fractional spectral series')
plt.xlabel('fractional difference dimension')
plt.ylabel('Pearson coefficient')
y = mean[0]
m = mean[3]
s = std[3]
plt.plot(y,m,'m-')
plt.plot(y,m+s,'y--')
plt.plot(y,m-s,'y--')
plt.savefig('pearson_spectral.pdf')
#plt.show()
plt.clf()
#%%
Q = []
for i in range(1000):
    X = ARMA.ar(d,N)
    Q.append(X)