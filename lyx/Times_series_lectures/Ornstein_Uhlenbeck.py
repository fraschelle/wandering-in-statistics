#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  2 16:15:54 2019

@author: francois
"""
from math import gamma
import numpy as np
import matplotlib.pyplot as plt
#%%
    N = 50000
    alpha = 0.5
    mu = 10
    X = [np.random.randn()+1]
    for i in range(1,N):
        X.append(mu*alpha+(1-alpha)*X[-1]+np.random.randn())
    plt.plot(X)
    print(np.mean(X))
    print(np.std(X))
#%%
    a = 0.2      # a float
    b = 0.9       # a float
    p = 15         # an integer
    q = 80        # an integer
    N = 15000      # total number of points generated, N > max(p,q)
    X = arma_rand(a,b,p,q,N)
    plt.plot(X)
    print(np.mean(X))
    print(np.std(X))

#%%