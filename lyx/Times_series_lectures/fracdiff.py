#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 15 14:40:33 2019

@author: francois
"""
import numpy as np
from math import gamma
#%%
def fracdiff(x,d):
    """
    Apply a fractional differentiation of order d to a time series x (a 1D numpy.array)
    Does not withdraw the first terms of the series, since it uses a circular convolution trick and some fast Fourier transform algorithm due to Jensen and Nielsen : 
        - Jensen and Nielsen, Journal of time series analysis 35 p.426-438 (2014)
            -> https://doi.org/10.1111/jtsa.12074
    So the first terms are untrusty in the following. 
    For a trustfull algorithm, use 'nablafrac' instead.
    Source : 
        https://www.patreon.com/posts/fractional-21878207
        https://github.com/SimonOuellette35/FractionalDiff/blob/master/question2.py
    """
    T = len(x)
    N = int(2**np.ceil(np.log2(2*T-1)))
    k = np.arange(1,T)
    b = (1,) + tuple(np.cumprod((k-d-1)/k))
    z = (0,)*(N-T)
    z1 = b + z
    z2 = tuple(x) + z
    dx = np.fft.ifft(np.fft.fft(z1)*np.fft.fft(z2))
    X = np.real(dx[0:T])
    return(X)

#%%
def get_weight_ffd(differencing_amt, threshold, weight_vector_len):
    # from https://github.com/hudson-and-thames/mlfinlab/blob/fracdiff/mlfinlab/fracdiff/fracdiff.py
    weights = [1.]
    k = 1
    ctr = 0
    while True:
        weight_ = -weights[-1] / k * (differencing_amt - k + 1)
        if abs(weight_) < threshold:
            break
        weights.append(weight_)
        k += 1
        ctr += 1
        if ctr == weight_vector_len - 1:
            break
    weights = np.array(weights[::-1]).reshape(-1, 1)
    return weights


def frac_diff_ffd(price_series, differencing_amt, threshold=1e-5):
    # from https://github.com/hudson-and-thames/mlfinlab/blob/fracdiff/mlfinlab/fracdiff/fracdiff.py
    # compute weights for the longest series
    weights = get_weight_ffd(differencing_amt, threshold, len(price_series))
    width = len(weights) - 1

    # apply weights to values
    output = []
    output.extend([0] * width)
    for i in range(width, len(price_series)):
        output.append(np.dot(weights.T, price_series[i - width:i + 1])[0])
    return np.array(output)

#%%
def nabla(X,n):
    """
    Apply a filter (1-B)**n of order n to an ARMA series.
    Suppress the first n terms of the initial series.
    Such a filter acts exactly as a derivative.
    Input : 
        X : a list ar a 1D numpy.array
        n : an integer
    Output : a list of size len(X)-n
    """
    Z = []
    for i in range(0,len(X)):
        z = [(-1)**k*gamma(n+1)/gamma(k+1)/gamma(n-k+1)*X[i-k] for k in range(0,n+1)]
        Z.append(np.sum(z))
    return(Z)
