fracdiff <- function(x, d){
  +     iT <- length(x)
  +     np2 <- nextn(2*iT - 1, 2)
  +     k <- 1:(iT-1)
  +     b <- c(1, cumprod((k - d - 1)/k))
  +     dx <- fft(fft(c(b, rep(0, np2-iT)))*fft(c(x, rep(0, np2-iT))), inverse=T)/np2;
  +     return(Re(dx[1:iT]))
  +     }

x = c(0,1,2,3,4,5,6,7,8,9)

fracdiff(x,2)
length(x)

v = x*x
fracdiff(x,2)

v = v*v
fracdiff(v,2)
fracdiff(v,1.2)
fracdiff(v,0.48)
