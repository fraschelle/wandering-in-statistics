#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass revtex4-1
\options notitlepage,rmp,preprint,tightenlines,nofootinbib,eqsecnum,longbibliography
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_author "Francois Konschelle [fraschelle(at)free.fr]"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks true
\pdf_backref false
\pdf_pdfusetitle true
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Time series
\end_layout

\begin_layout Author
François Konschelle
\end_layout

\begin_layout Date
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
today
\end_layout

\end_inset


\end_layout

\begin_layout Standard
A stochastic process 
\begin_inset Formula $X_{t}$
\end_inset

 depending on the continuous parameter 
\begin_inset Formula $t$
\end_inset

, is said to be self-similar with self-similarity parameter 
\begin_inset Formula $H$
\end_inset

 if 
\begin_inset Formula $X_{t}$
\end_inset

 and 
\begin_inset Formula $c^{-H}X_{ct}$
\end_inset

 follows the same statistical distribution, for any 
\begin_inset Formula $c\in\mathbb{R}$
\end_inset

.
\end_layout

\begin_layout Section
R/S test
\end_layout

\begin_layout Standard
Suppose a series 
\begin_inset Formula $X_{t}$
\end_inset

.
 Define 
\begin_inset Formula 
\begin{equation}
S^{2}\left(t,d\right)=\sum_{i=1}^{t+d-1}\frac{X_{i}^{2}}{d}-\left[\bar{X}\left(t,d\right)\right]^{2}
\end{equation}

\end_inset


\begin_inset Formula 
\begin{equation}
R\left(t,d\right)=\max_{1\leq u\leq d}\left[\sum_{j=t}^{t+u-1}X_{j}-u\bar{X}\left(t,d\right)\right]-\min_{1\leq u\leq d}\left[\sum_{j=t}^{t+u-1}X_{j}-u\bar{X}\left(t,d\right)\right]
\end{equation}

\end_inset


\begin_inset Formula 
\begin{equation}
\bar{X}\left(t,d\right)=\sum_{i=t}^{t+d-1}\frac{X_{i}}{d}
\end{equation}

\end_inset

the ratio 
\begin_inset Formula $R\left(t,d\right)/S\left(t,d\right)$
\end_inset

 is called the rescaled range.
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "../../../../library"
options "apsrmp4-1"

\end_inset


\end_layout

\end_body
\end_document
