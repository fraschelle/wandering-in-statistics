#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass revtex4-1
\options notitlepage,rmp,preprint,tightenlines,nofootinbib,eqsecnum,longbibliography
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_author "Francois Konschelle [fraschelle(at)free.fr]"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Notes on econometrics, financial mathematics, algorithmic finance
\end_layout

\begin_layout Author
François Konschelle
\end_layout

\begin_layout Date
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
today
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Section
Financial theories
\end_layout

\begin_layout Subsection
Efficient market theory
\end_layout

\begin_layout Subsection
Dow theory
\end_layout

\begin_layout Subsection
Brownian motion theory
\end_layout

\begin_layout Standard
Suppose a continuous-time stochastic process.
\end_layout

\begin_layout Section
Financial instruments
\end_layout

\begin_layout Subsection
Assets
\end_layout

\begin_layout Standard
Bond, Commodity, Derivatives, Foreign exchange, Money, Over-the-counter,
 Private equity, Real estate, Spot, Stock
\end_layout

\begin_layout Subsection
References
\end_layout

\begin_layout Standard
Wikipedia pages : 
\end_layout

\begin_layout Section
Portfolio management
\end_layout

\begin_layout Subsection
References
\end_layout

\begin_layout Standard
Modern portfolio theory : 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
cite{Markowitz1952}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Wikipedia pages : 
\begin_inset CommandInset href
LatexCommand href
name "Modern portfolio theory"
target "https://en.wikipedia.org/wiki/Modern_portfolio_theory"

\end_inset

, 
\begin_inset CommandInset href
LatexCommand href
name "Portfolio optimization"
target "https://en.wikipedia.org/wiki/Portfolio_optimization"

\end_inset

, 
\begin_inset CommandInset href
LatexCommand href
name "Trading strategy"
target "https://en.wikipedia.org/wiki/Trading_strategy"

\end_inset

, 
\end_layout

\begin_layout Section
Financial markets
\end_layout

\begin_layout Subsection
Trading / exchange mechanisms
\end_layout

\begin_layout Subsection
References
\end_layout

\begin_layout Standard
Wikipedia pages : 
\begin_inset CommandInset href
LatexCommand href
name "Financial markets"
target "https://en.wikipedia.org/wiki/Financial_market"

\end_inset

, 
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "../../../library"
options "apsrmp4-1"

\end_inset


\end_layout

\end_body
\end_document
